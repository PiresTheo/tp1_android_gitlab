package pires.theo.tpone;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class AfterValiderActivity extends AppCompatActivity {
    private ListView mListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.aftervalider_main);
        Toast.makeText(getApplicationContext(), "Bienvenue " + MainActivity.nom, Toast.LENGTH_SHORT).show();

        mListView = (ListView) findViewById(R.id.listView);

        List<Contact> contacts = genererContacts();

        ContactAdapter adapter = new ContactAdapter(AfterValiderActivity.this, contacts);
        mListView.setAdapter(adapter);
    }

    private List<Contact> genererContacts() {
        List<Contact> contacts = new ArrayList<Contact>();
        contacts.add(new Contact(R.drawable.logo_contact_blue, "Pires", "Theo", "06.33.47.50.81", "theopires99@gmail.com", "Maubeuge"));
        contacts.add(new Contact(R.drawable.logo_contact_red, "Pires", "Thomas", "06.33.47.50.81", "theopires99@gmail.com", "Maubeuge"));
        contacts.add(new Contact(R.drawable.logo_contact_green, "Pires", "Thea", "06.33.47.50.81", "theopires99@gmail.com", "Maubeuge"));
        contacts.add(new Contact(R.drawable.logo_contact_yellow, "Pires", "To", "06.33.47.50.81", "theopires99@gmail.com", "Maubeuge"));
        contacts.add(new Contact(R.drawable.logo_contact_purple, "Pires", "Tom", "06.33.47.50.81", "theopires99@gmail.com", "Maubeuge"));

        return contacts;
    }
}