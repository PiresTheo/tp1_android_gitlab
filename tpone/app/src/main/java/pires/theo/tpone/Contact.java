package pires.theo.tpone;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.Image;
import android.widget.ImageView;

public class Contact {
    //visible
    private int photo;
    private String nom;
    private String prenom;
    private ImageView favori;
    //non visible
    private boolean favoriBool;
    private String numero;
    private String mail;
    private String ville;


    public Contact(int photo, String nom, String prenom, String numero, String mail, String ville) {
        this.photo = photo;
        this.prenom = prenom;
        this.numero = numero;
        this.mail = mail;
        this.ville = ville;
    }

    public int getPhoto() { return photo; }

    public void setPhoto(int photo) { this.photo = photo; }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public ImageView getFavori() {
        return favori;
    }

    public void setFavori(ImageView favori) {
        this.favori = favori;
    }

    public String getNumero() { return numero; }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public void setFavoriBool(boolean favoriBool) {
        this.favoriBool = favoriBool;
    }
}
