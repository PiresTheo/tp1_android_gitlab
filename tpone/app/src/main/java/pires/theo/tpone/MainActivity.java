package pires.theo.tpone;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    private Boolean firstTime = null;
    public static String nom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (isFirstTime()) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);

            final EditText tf = findViewById(R.id.Nom);
            final Button button = findViewById(R.id.ButtonValider);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!tf.getText().toString().isEmpty()) {
                        nom = tf.getText().toString();
                        Intent intent = new Intent( MainActivity.this, AfterValiderActivity.class);
                        startActivity(intent);
                    }
                }
            });

        } else {
            Intent intent = new Intent( MainActivity.this, AfterValiderActivity.class);
            startActivity(intent);
        }
    }

    private boolean isFirstTime() {
        if (firstTime == null) {
            SharedPreferences settings = this.getSharedPreferences("first_time", Context.MODE_PRIVATE);
            firstTime = settings.getBoolean("firstTime", true);
            if (firstTime) {
                SharedPreferences.Editor editor = settings.edit();
                editor.putBoolean("firstTime", false);
                editor.commit();
            }
        }
        return firstTime;
    }
}